Title:

How to NOT get insane when working for 20 years as a dev - personal mental health tricks.

Abstract:

I've been designing and building software for the last 20 years, and in that time I've lost a lot of friends due to burnout and depression. A lot of others have moved to management, become POs, Scrum Masters or full-time celebrities. When talking with them most say that writing software became too difficult for them. Which is strange, because from my perspective, programming is much easier than 20 years ago.

I'm not a psychologist, I have no proper education or skills to help you out, but I've survived 20 years, and I'm no more insane than I was back then, so maybe I can tell you what have helped me survive, and be even more happy in a software dev job. If it helps one person not become a manager, I'll consider my job done.

