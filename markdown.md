name: inverse
layout: true
class: inverse

---
layout: false
background-image: url(img/nge2.png)
background-size: 85%
background-repeat: no-repeat
background-position: center middle
class: top left

#### How to NOT get insane when working for 22 years as a dev

#### personal mental health tricks

by Jakub Nabrdalik

---
layout: false
background-image: url(img/69797168_10157781165793826_1704987809036632064_n.jpg)
background-size: 50%
background-repeat: no-repeat
background-position: center middle
class: left bottom


???

Wykładowca na UWMie w Olsztynie, 2000:

"Programowanie to ślepa uliczka bo i tak nie będziecie w stanie nadążyć za postępującym tempem rozwoju technologii, wypalicie się zawodowo, dostaniecie depresji, i w najlepszym wypadku będziecie uczyć przestarzałych rzeczy na uczelni."

Kortowo w Olsztynie to były szpital psychiatryczny

---

layout: false
background-image: url(img/IMG_20220701_140159.jpg)
background-size: 100%
background-repeat: no-repeat
background-position: center middle
class: left bottom

22 years designing, programming and keeping it working on production

???

Nazywam się Jakub Nabrdalik i od 22 lat projektuję, implementuję i utrzymuję przy życiu systemy. Od 13 lat prowadzę również wykłady o architekturze, programowaniu praktycznie tylko i wyłącznie opowiadając na podstawie swoich doświadczeń z produkcji.

W tym roku mam dość dramatyczna NDA, więc temat który poruszę będzie z goła inny.  Będę mówił o profilaktyce zdrowia psychicznego, czyli co zrobić żeby nam nie odpierdoliło podczas programowania.


---

background-image: url(img/szklanka.png)
background-size: 80%
background-repeat: no-repeat
background-position: center middle
class: top left

???

W sieci można znaleźć całkiem sporo dobrych porad jak o siebie zadbać, na przykład takich

Proste i skuteczne. Na obrazku najszczęśliwszy pracownik miesiąca w korporacji. 

---

background-image: url(img/fake-skeletons-underwater-6.jpg)
background-size: 100%
background-repeat: no-repeat
background-position: center middle
class: top left

#### The most happy employee of the month

---

background-image: url(img/anita.jpeg)
background-size: 50%
background-repeat: no-repeat
background-position: top right
class: bottom left


Mamy psychologów w IT

(po prawej Anita Przybył)

SWPS 2020: Psychologia i informatyka

???

Żarty na bok, mamy sporo w naszym świadku psychologów, na przykład Anita Przybysz którzy opowiedzą o zdrowiu psychicznym, depresji i wypaleniu zawodowym zdecydowanie lepiej niż jakiś random z internetu, czy dziwak bez szkoły, na przykład ja.

SWPS w 2020 uruchomił kierunek: Psychologia i Informatyka, więc mamy nawet dedykowane kształcenie w kierunku który nas interesuje w tym talku.

---

layout: false
background-image: url(img/d059e9fa70887bb6.jpg)
background-size: 100%
background-repeat: no-repeat
background-position: center middle
class: left bottom

Random dude from a conference

???

Gorąco polecam profesjonalną pomoc. 


---

layout: false
background-image: url(img/am730rj_700bwp.webp)
background-size: 60%
background-repeat: no-repeat
background-position: center left
class: top right

Endogenous depression

Bad neurotransmitters

Hardware is broken

It's not your fault

???

Zwłaszcza że można mieć depresję endogenną, która nie została wywołana traumatycznymi doświadczeniami z przeszłości, nie ma nic wspólnego z młodym dynamicznym zespołem w pracy, albo szefem-psychopatą, a wynika z nieprawidłowej czynności neuroprzekaźników w ośrodkowym układzie nerwowym. Czyli krótko mówiąc, hardware się zepsuł i żadne pierdolenie o szklance wody i ograniczeniu stresu w pracy Wam nie pomoże. 

Pamiętajcie, bycie pojebanym nie musi wynikać z waszych błędnych decyzji, możecie mieć po prostu popsute CPU.

---

layout: false
background-image: url(img/LANDSCAPE_840.jpeg)
background-size: 100%
background-repeat: no-repeat
background-position: top center
class: bottom center

psychotherapist -> psychologist -> psychiatrist -> neurologist

Instytut Psychiatrii i Neurologii na Sobieskiego

???

Ba nawet psychoterapeuta Wam nie pomoże, trzeba pójść do prawdziwego psychologa, który jak już się podda, to wyśle Was do prawdziwego psychiatry, który wyśle Was do Instytutu Psychiatrii i Neurologii na Sobieskiego, gdzie metodą prób i błędów dobiorą Wam chemię żeby ten zepsuty hardware skompensować.

---

layout: false
background-image: url(img/Dr_Amen_Dr_Johnson_Square.webp)
background-size: 75%
background-repeat: no-repeat
background-position: center left
class: middle right

Daniel Amen

brain scan

???

Jeśli będziecie mieli dużo pieniędzy do wydania albo dużo szczęścia, to może nawet uda się Wam nawet trafić na jednego z niewielu psychiatrów, takich jak Daniel Amen, który zamiast bawić się we wróżenie z fusów, zainwestuje w prawdziwe observability i wykorzysta tomografię by zrobić Wam skan mózgu, bo wasze problemy psychiczne są często fizycznie widoczne na skanie.

---

layout: false
background-image: url(img/psychol.jpg)
background-size: 60%
background-repeat: no-repeat
background-position: center middle
class: center bottom

Konflikt interesów?

???

Ale podobnie jak przy debugowaniu systemu na produkcji, wasze szanse na profesjonalne podejście do observability są raczej marne i pewnie skończy się cotygodniowych spotkaniach z terapeutą w wygodnym fotelu za 250zł/h po których nic się nie zmienia. Psychoterapeuci o dziwo nie dostają pieniędzy za wyleczenie człowieka, wręcz przeciwnie, czym dłużej jest chory, tym więcej zarabiają, więc jakby to ująć - mają lekki konflikt interesów.

---

layout: false
background-image: url(img/ad_170553675.jpg)
background-size: 70%
background-repeat: no-repeat
background-position: middle right
class: center bottom

Guys would rather see a proctologist

???

Całkiem też prawdopodobne że, przynajmniej w przypadku panów, nie pójdziecie na czas do specjalisty, tylko będziecie wszystko kisić w sobie, aż pewnego pięknego dnia coś pęknie i spubujecie bungee jumping z mostu tylko bez bungee, bo wiadomo oszczędności czasu i pieniędzy. Wszak u panów częściej znajdziemy  na półce ilustrowany Atlas technik operacyjnych w proktologii niż jakiekolwiek czasopismo psychologiczne.

---

layout: false
background-image: url(img/TANITA_TIKARAM_ANCIENT+HEART-490597.jpg)
background-size: 50%
background-repeat: no-repeat
background-position: middle right
class: left bottom


???

Pomoc profesjonalną pozostawiam profesjonalistom. Ja mogę mówić tylko o swoim doświadczeniu. A jest ono następujące.

Mam 42 lata. 15 lat życia spędziłem mieszkając z kobietami ze zdiagnozowaną depresją. Miałem nawet psa z depresją.

W takich okolicznościach musiałem sobie zadać pytanie: czy ja przypadkiem nie jestem przyczyną tych wszystkich depresji?

Na całe szczęście (dla mnie) z wyjątkiem mojej mamy i najmłodszej siostry, wszystkie te kobiety miały depresję zanim mnie poznały, więc to hipotezę można odrzucić.

Sławek Sobótka wysnuł inną, może ja podświadomie szukam w życiu kobiet z depresją.

Jak byłem nastolatkiem moją ulubioną piosenkarką była Tanita Tikaram, która miała najsmutniejszy głos na świecie i najsmutniejszą płytę debiutancką, więc cholera wie...


---


layout: false
background-image: url(img/pdch38bbe5911.jpg)
background-size: 50%
background-repeat: no-repeat
background-position: middle right
class: left bottom

???

Kolejne pytanie, czy ja nie mam depresji? Czy w ogóle byłbym w stanie rozpoznać u siebie depresję?

No i po głębokiej analizie, mogę stwierdzić że nie, nigdy nie miałem depresji, i choć kilka razy w życiu byłem w sytuacji prowadzącej na dłuższą metę do wypalenia zawodowego, nigdy również nie miałem wypalenia.

---

layout: false
background-image: url(img/goodatdepression.jpg)
background-size: 40%
background-repeat: no-repeat
background-position: middle center
class: center bottom

Mental health safety, or tricks at work to not get burned out

What works for me, might kill you

This talk is to motivate you to search for what works for you

To find your your mental tricks before it's too late

???

Ale w sumie dlaczego?

Dzieląc przez 15 lat codzienność z osobami w depresji  tematem naszych rozmów są często sposoby radzenia sobie z trudnościami w życiu i tak sobie rozmawiając odkryłem że mam sporo wbudowanych zachowań i mechanizmów które albo pomagają mi przetrwać trudne chwile, albo mnie regularnie wyciągają z niebezpiecznych sytuacji. Chciałbym się nimi z Wami tym podzielić, zaznaczając że nie jestem psychologiem, co działa dla mnie może być zabójcze dla Ciebie, i generalnie "don't do it at home". Czyli będzie to raczej talk motywacyjny o profilaktyce wypalenia zawodowego, żebyście świadomie poszukali swoich mechanizmów kiedy się jeszcze czujecie dobrze, zamiast szukać ich będąc już na dnie. Albo żebyście byli w stanie zauważyć niebezpieczne sytuacje na zawodowym horyzoncie, zanim wdepniecie w gówno.

---

layout: false
background-image: url(img/artworks-000095466306-ci3bto-t500x500.jpg)
background-size: 60%
background-repeat: no-repeat
background-position: bottom right
class: left top

# How does the software work?

### An inaccurate model <br /> pulled out of an ass

???

Jak działa mózg i świadomość?

Przyjmijmy uproszczony model naszej wirtualnej maszyny, czyli mózgu. Model, jak każdy model, będzie absolutnie błędny, ale pozwoli nam zbudować w sobie intuicję do dalszej rozmowy na temat zdrowia, intuicję której bardzo potrzebowałem ale jej nie miałem mając lat dziesięć.


Będziemy bazować na dwóch przykładowych doświadczeniach: 

---

layout: false
background-image: url(img/brainTMS.png)
background-size: contain
background-size: 70% auto
background-repeat: no-repeat

## Consciousness only narrates made decisions

???

Professor Alvaro Pascual-Leone at Harvard

> They used transcranial magnetic stimulation (TMS), which discharges a magnetic pulse and excites the area of the brain underneath, to stimulate the motor cortex and initiate movement in either the left or right hand. 

> Even after an experimenter manipulates a choice by stimulating the brain, participants often claim that their decision was freely chosen.

---

layout: false
background-image: url(img/bridge.jpeg)
background-size: contain
background-repeat: no-repeat
background-position: center right

.left-column[

##  Brain fails to understand the source of emotions

"Some evidence for heightened sexual attraction under conditions of high anxiety." Dutton & Aron, 1974

]

???

> 85 male passersby were contacted either on a fear-arousing suspension bridge or a non-fear-arousing bridge by an attractive female interviewer who asked them to fill out questionnaires containing Thematic Apperception Test (TAT) pictures. Sexual content of stories written by Ss on the fear-arousing bridge and tendency of these Ss to attempt postexperimental contact with the interviewer were both significantly greater. No significant differences between bridges were obtained on either measure for Ss contacted by a male interviewer.



---

layout: false
background-image: url(img/Phineas_Gage_Cased_Daguerreotype_WilgusPhoto2008-12-19_EnhancedRetouched_Color.jpg)
background-size: 65%
background-repeat: no-repeat
background-position: right center
class: left top

# Sleeping </br> hemisphere

???

1 Neurolodzy uśpili jedną hemisferę mózgu,  i pacjent zaczął wykazywać zupełnie inną osobowość (z introwertyka stał się ekstrawertykiem). Co sugeruje że w różne części naszych mózgów mają swoje własne upodobania (charaktery, czy mają osobne świadomości to już dużo trudniejszy temat i zdania neurologów wydają się podzielone).


---

layout: false
background-image: url(img/Health-2.jpg)
background-size: 100%
background-repeat: no-repeat
background-position: middle bottom
class: left top

???

W efekcie nasz model będzie wyglądał tak:

### świadomość 
proces jednowątkowy, kontroluje nasz wewnętrzny głos, możemy prześledzić jego tok rozumowania (myśli), podejmuje wysokopoziomowe decyzje strategiczne, nie podejmuje decyzji taktycznych (nie decyduje o niczym, buduje tylko spójny obraz siebie samego i namawia inne procesy w mózgu żeby coś zrobiły)

### podświadomość 
duża pula procesów równoległych które analizują sytuację, podejmują decyzje taktyczne i wykonują robotę. Mają swoje własne "upodobania", "przekonania" i nie muszą się ze sobą zgadzać. Nie możemy śledzić toku ich rozumowania (black box). Świadomość dostaje od nich sygnały (np. emocje) często pośrednio przez zachowania ciała  (np. nie odróżniasz zakochania od strachu). 

Intuicja to nic innego jak procesowanie przez podświadomość. Nie wiemy dlaczego coś sądzimy, ale coś sądzimy (i to często prawidłowo).

---

# Hey but this is wrong!

> Marvin Minsky's "Society of Mind" model claims that mind is built up from the interactions of simple parts called _agents_, which are themselves mindless.

> Thomas R. Blakeslee described the brain model which claims that _brain is composed of hundreds of independent centers of thought called "modules"_.

> Michio Kaku described the brain model using the analogy of _large corporation_ which is controlled by _CEO_.

> Neurocluster Brain Model describes the brain as a massively parallel computing machine [...] The neurocluster which most of the time has the access to actuators [...] is called the _main personality_. Other neuroclusters [...] are called "autonomous neuroclusters".


It is. But it's a model to build intuition, not represent reality.

---

# Model Wewnętrznych Systemów Rodzinnych

> Łączy [...] myślenie systemowe z podejściem, w którym umysł składa się z półjawnych podświadomości. Każda z nich charakteryzuje się indywidualnym punktem widzenia i cechami. 
>
> [wikipedia](https://pl.wikipedia.org/wiki/Model_Wewn%C4%99trznych_System%C3%B3w_Rodzinnych)

???

Jest Model Wewnętrznych Systemów Rodzinnych (Internal Family Systems Model) i terapia na nim bazująca.


---

#  Teoria Dialogowego Ja

> "Rozbieżności, a nawet sprzeczności między _głosami_ wewnątrz Ja są traktowane jako zjawisko konstruktywne, jako nieodłączna cecha zdrowego funkcjonowania Ja, przyczyniająca się do rozwoju (mimo iż czasem mogą wiązać się z lękiem i depresją). Szkodliwy jest natomiast brak dialogu, uciszanie jednych _głosów_ przez inne."
> 
> https://pl.wikipedia.org/wiki/Dialogowe_Ja

???

Jest też teoria dialogowego ja (Dialogical Self Theory) i metody terapii na niej bazujące. 

Jak psychologowie mogą wykorzystywać tą koncepcję do terapii, to my możemy użyć jej w modelu z dupy.

---

layout: false
background-image: url(img/20190905-Neon-Genesis-Evangelion.jpg)
background-size: 100%
background-repeat: no-repeat
background-position: middle bottom
class: left top


???

Kluczowe dla mnie było przyjęcie założenia, że mam w głowie całą zespół którym trzeba się opiekować.

---

# Take care of the team inside your head

I can have conflicting needs and emotions, that consciousness receives

I can misinterpret my emotions or needs

My consciousness chooses what should I do with my emotion. Should I let it take over, should I ignore it, should I strengthen it?

???

- mogę mieć wewnętrznie sprzeczne emocje i potrzeby, które po prostu docierają do świadomości
- mogę źle interpretować swoje emocje lub potrzeby, bo świadomość nie ma dostępu bezpośredniego do wnioskowania podświadomości, jedynie interpretuje sygnały
- to ja świadomie wybieram co z emocją lub potrzebą zrobię - czy będą ją ignorował czy pozwolę się jej rozwinąć, czy podążę za nią

---

layout: false
background-image: url(img/gomjabbar.png)
background-size: 80%
background-repeat: no-repeat
background-position: center bottom
class: left top


# Take care of the team inside your head

I can have conflicting needs and emotions, that consciousness receives

I can misinterpret my emotions or needs

My consciousness chooses what should I do with my emotion. Should I let it take over, should I ignore it, should I strengthen it?

???

Dla mnie to jest właśnie definicja człowieka, swoisty test Gom Jabbar, zwierzę (w rozumieniu Diuny Franka Herberta) pozwala podświadomej emocji decydować o zachowaniu, człowiek świadomie podejmuje decyzję czy podąża za emocją, czy ją wzmacnia, czy ją ucisza.

> "_It kills only animals, Let us say I suggest you may be human. Steady! I warn you not to try jerking away. I am old, but my hand can drive this needle into your neck before you can escape me._"


---

# Single body problem


> Szkodliwy jest natomiast brak dialogu, uciszanie jednych _głosów_ przez inne."
> https://pl.wikipedia.org/wiki/Dialogowe_Ja

Ignore your subconscious personality long enough, and it will rebel


???

Uciszanie emocji rodzi pewne niebezpieczeństwo. Co się stanie jeśli przez lata ignorujesz jedną osobowość w swojej głowie, która zapala czerwone światełko i mówi "moja granice nie są respektowane". W pewnym momencie ta osobowość może Ci rzucić wyjątkiem podczas wstawania systemu rano z łóżka i powiedzieć: ignorowałeś mnie zasrańcu przez tyle lat, to ja teraz będę leżeć i nie pozwolę całemu systemowi ruszyć. Depresje mają różne objawy, ale problem z przekonaniem siebie samego by wstać z łóżka jest dość popularny. Zawsze kiedy obserwowałem taką sytuację, miałem wrażenie że któraś z wewnętrznych osobowości się buntuje bo nie zaopiekowaliśmy się wcześniej jej potrzebami.

W IT mamy predyspozycję do skupiania się na naszej świadomości i ignorowania sygnałów podświadomości. Często się mówi o nerdach że nie czytają sytuacji, ale jeszcze gorzej jest z czytaniem wewnętrznych emocji i potrzeb. U siebie na przykład odkryłem że świadomość jaką emocję aktualnie odczuwam przychodzi mi kilka minut później niż mojej dziewczynie. Co więcej, często w ogóle nie przychodzi.


---

layout: false
background-image: url(img/zdjecie-nr-1.png)
background-size: 100%
background-repeat: no-repeat
background-position: center middle
class: left top

???

Można też oszukać świadomość. Ludzie chodzą na masaż relaksacyjny, który poprzez czysto fizyczne rozluźnienie i rozbicie mięśni powoduje że świadomość interpretuje ten stan jako: jestem zrelaksowany. Pamiętajcie że świadomość nie wie co robi podświadomość, jedynie interpretuje sygnały.

---

layout: false
background-image: url(img/pulpfiction.jpeg)
background-size: 60%
background-repeat: no-repeat
background-position: center middle
class: left top

???

Dla mnie to nie działa. 30 lat trenuję siatkówkę, mój mózg nie łączy napięcia mięśni, bólu fizycznego ze stresem lub emocjami. Dla mnie zakwasy i ból mięśni łączą się z poczuciem szczęścia. Za to masaże mnie stresują. Jakiś obcy człowiek ma dotykać przez godzinę mojego ciała, brrrr.... nie dziękuję.

Moja dziewczyna mówi: czuję tu w brzuchu i w mięśniach karku że jestem dziś bardzo napięta i nerwowa.

Jedyne co ja w brzuchu czuję, to wczorajszego kebaba na ostrym. 

---

layout: false
background-image: url(img/Health-3.jpg)
background-size: 85%
background-repeat: no-repeat
background-position: center bottom
class: left top

# Contradicting signals and bad interpretation

???

Popatrzmy teraz co wynika z naszego przyjętego modelu.

Co jeśli ignorujesz jedną osobowość: depresja wygląda jakby ktoś wcisnął hamulec ręczny

Ekstrawertycy w depresji są zdziwieni że muszą ograniczać sobie kontakty, nawet gdy tego nie chcą. 
Podobnie wypaleni programiści - chcą, ale nic nie mogą (hamulec). Pair programming trochę działa bo oszukuje.

Trzeci dan i czarny pas w karate - student prawa który nigdy nie lubił karate.
Koleżanka która była prawnikiem, i po aplikacji poszła w programowanie
Kupiłem dom pod lasem, tylko kurwa nienawidzę lasu

---

layout: false
background-image: url(img/emotions.png)
background-size: 95%
background-repeat: no-repeat
background-position: center middle
class: left top

---

# Sadness

Important information for our thriving: 

> *Something important for me is missing*

Action needed: 

> *Need to figure out how to find it in my life*

Examples of action: 

> *Missing connection - talk to somebody, open up in my friendships, or find new friendships. Reconnect with your hobbies, activities that are valuable for you*

--

IT examples: 

> developer becoming manager, no time for programming, context switching vs deep work

> changing great client after 4y (VF)

???

Często spotykam: dev idzie w mgm i nie ma czasu na programowanie, ale to jest niezgodne z jego wewnętrzną potrzebą z tym jak o sobie myśli. Ja byłem w mgm, szkolę ludzi, ktoś tam przychodzi na moje talki, ale na twitterze w opiesie siebie jak tylko wpisałem "Software Dev" to nagle poczułem wewnętrzny spokój.

Pamiętaj: jeśli ignorujesz wewnętrzną potrzebę jednej z osobowości, to może wcisnąć hamulec

Z mojej perspektywy: 
Co 3 lata zmiana firmy/projektu. 4 rok w VF męczyłem się jak cholera, jak dołączyłem do firmy która jest w dupie thrill wrócił - jestem architektem i muszę mieć co projektować. Gdy wszystko jest już zaprojektowane i zbudowane, mój wewnętrzny architekt wciska hamulec. Jednocześnie potrzebuję minimum 1 rok na produkcji by czuć się ok z tym co zbudowałem. I to mi się notorycznie powtarza.
Zawsze myślałem że lubię bezpieczeństwo, a tymczasem potrzebuję pewnego poziomu wyzwania albo mam dość. Ale za wysoki poziom zjebania firmy i szlag mnie trafia.


---

# Anger 

Important information for our thriving: 

> *My boundaries are being crossed*

Action needed: 

> *Need to communicate and respect my boundaries*

Examples of action: 

> Say no to someone taking advantage. Tell your partner what you need. Make requirements at work. Tell others how you feel.


---

layout: false
background-image: url(img/keanu.png)
background-size: 50%
background-repeat: no-repeat
background-position: center middle
class: left top


---


# Anger 

Important information for our thriving: 

> *My boundaries are being crossed*

Action needed: 

> *Need to communicate and respect my boundaries*

Examples of action: 

> Say no to someone taking advantage. Tell your partner what you need. Make requirements at work. Tell others how you feel.

IT examples: 

> Micromanagement kills creativity

> Enforced estimations

> Open Space

---

layout: false
background-image: url(img/2941b4d66e16a202.jpg)
background-size: 50%
background-repeat: no-repeat
background-position: center middle
class: left top

???

Historia o szacowaniu
Kolo (mgm) na konfie - co zrobić z człowiekiem który odmawia szacowania zadań. Ja to rozumiem, sam odmawiam. Nie mamy danych wejściowych żeby cokolwiek szacować, to metoda nienaukowa, wyciąganie liczb z dupy, moja intuicja nic nie mówi, czuję wewnętrzny protest.
Ale jeśli powiem "fuck off" to manager może mnie zwolnić. W jakimś celu ten człowiek tego potrzebuje.
Po co Ci szacowanie? Żeby móc samsungowi powiedzieć że musi przesunąć dostawę o pół roku, bo inaczej zerwie ze mną umowę. Ok, czyli do ograniczenia ryzyka? Tak. No to zróbmy najtrudniejsze zadanie najpierw, pozostałe są łatwiejsze, szybciej się dowiemy czy damy radę czy nie. Ale mój szef też potrzebuje szacunków bo inaczej nie może planować? Czego planować? Bo inaczej jest zły. Acha, czyli teraz musimy sobie radzić z wewnętrznym strachem szefa przed utratą kontroli i robimy to przy pomocy wymyślonych liczb z dupy które nie mają jakiejkolwiek wartości? To może wyślijmy szefa do psychologa, albo rzucajmy kością?

---

layout: false
background-image: url(img/signal-2023-05-31-22-09-01-947.jpg)
background-size: 65%
background-repeat: no-repeat
background-position: center middle
class: left top

# Open Space vs ancient japanese wchujtuj

<img src="img/pobrane.jpeg"/>

???

Open space vs wchujtuj


---

# Frustration

Important information for our thriving: 

> *What I’m doing is not working for me*

Action needed: 

> *Need to find a new strategy*

Examples of action: 

> Ask for another type of tasks at work. Learn to say no to too many arrangements. Get guidance, therapy, coaching for new tools and strategy.

IT examples: 
> simulating work, painting the grass green

> the story of a cloud provider

???

Developerzy się wkurwiają że nie mogą nic zmienić, ale jednocześnie nic nie zmieniają. Jeśli masz się wypisać z firmy, to może równie dobrze możesz spróbować coś zmienić bez pytania o pozwolenie?

Z mojej perspektywy: 
Touk i symulowanie pracy w T
AWS i marketing

---

# Fear

Important information for our thriving: 

> *Something is threatening my physical or mental wellbeing*

Action needed: 

> *Need more security*

Examples of action: 

> *Run or fight, but also: ask for help, gather more information; learn a new skill, say stop, or analyse the situation objectively*

IT examples: 
> a consultant that was afraid of going to production

---

# What you need to remove fear?

> Continuous Deployment

> Observability

> Microservices

> Autonomy

> FinSecDevOps

> TDD/BDD 

--

[Common mistakes when moving to microservices](https://www.youtube.com/watch?v=jo46-CP6ywU)

[Improving your Test Driven Development in 45 minutes](https://www.youtube.com/watch?v=2vEoL3Irgiw)

[Keep IT clean, mid-sized building blocks and hexagonal architecture](https://www.youtube.com/watch?v=ma15iBQpmHU)

[Requirements & BDD: The lost art of analysis and acceptance scenarios](https://www.youtube.com/watch?v=71fdXhlHXjE)

[Test Driven Traps](https://www.youtube.com/watch?v=wbAtJlbRhbQ)

???

W mojej rodzinie jestem drugim najbezpieczniej grającym po bracie, nie znoszę strachu.

Gdy chciałem wziąć wolne, wpadłem w panikę i nabrdałem konsultacji i szkoleń pod korek.

---

# What if you cannot fix your company?

---

layout: false
background-image: url(img/c7cfcdac82d2e727.jpg)
background-size: 40%
background-repeat: no-repeat
background-position: middle center
class: left top

# What if you cannot fix your company?

---

# What if you cannot fix your company?

How to play a lost game

> Change the goal

--

Escapism

> Escapism is mental diversion from unpleasant or boring aspects of daily life, typically through activities involving imagination or entertainment. Escapism also may be used to occupy one's self away from persistent feelings of depression or general sadness

> However, if permanent residence is taken up in some such psychic retreats, the results will often be negative and even pathological

> Choose your weapon wisely

---

layout: false
background-image: url(img/depression-meme-13.jpg)
background-size: 70%
background-repeat: no-repeat
background-position: middle center
class: left top

???

Brak możliwości zrelaksowania się zabija

Alkochol i seriale

Ja wolę siatkówkę

---

layout: false
background-image: url(img/354223200_10230232556535176_566326150275853695_n.jpg)
background-size: 50%
background-repeat: no-repeat
background-position: middle center
class: left top

???

Książki

---

layout: false
background-image: url(img/minecraft.jpg)
background-size: 60%
background-repeat: no-repeat
background-position: middle center
class: left top


---

layout: false
background-image: url(img/nmjks-cztery-srodki-1-scaled.jpg)
background-size: 100%
background-repeat: no-repeat
background-position: middle center
class: left top

#### https://kolorofanki.pl/

---

# What if you cannot fix your company?

How to play a lost game

> Change the goal

Escapism

> Escapism is mental diversion from unpleasant or boring aspects of daily life, typically through activities involving imagination or entertainment. Escapism also may be used to occupy one's self away from persistent feelings of depression or general sadness

> However, if permanent residence is taken up in some such psychic retreats, the results will often be negative and even pathological

> Choose your weapon wisely

Perfect moment

Cuddling

???

O nauce przegrywania. Zmiana celu, co robią szkoleniowcy by się nie stresować. Co robią siatkarze.

Stabilność i Eskapizm

Perfect Moment

Przytulanie

---

layout: false
background-image: url(img/decodedbrainactivity.png)
background-size: 70%
background-repeat: no-repeat
background-position: top center
class: left bottom


"Semantic reconstruction of continuous language from non-invasive brain recordings" Jerry Tang, Amanda LeBel, Shailee Jain & Alexander G. Huth.

Published: 01 May 2023

https://www.nytimes.com/2023/05/01/science/ai-speech-language.html



We can read conscious thought right out of your brain right now. 

Perhaps we will be able to read subconsciousness soon?

???

Don't like to listen to your subconsciousness?

Perhaps soon you won't have to...

AI makes non-invasive mind-reading possible by turning thoughts into text


---

layout: false
background-image: url(img/W-Duch-2014.jpg)
background-size: 50%
background-repeat: no-repeat
background-position: right center
class: left top

# What to read

.left-column[

"Brain - The story of you" 
<br /> David Eagleman

"Behave - The Biology of Humans at Our Best and Worst" 
<br /> Robert M. Sapolsky

prof. Włodzisław Duch: https://wduch.wordpress.com/
<br/> (Mózg i neuroinformatyka, Istota Świadomości)

"Jest OK. To dlaczego nie chcę żyć?" 
<br /> Marek Sekielski, Małgorzata Serafin
]

---

layout: false
background-image: url(img/1267119ba385c87e.jpg)
background-size: 60%
background-repeat: no-repeat
background-position: right bottom
class: left top

# Thanks, find your own hacks!

This presentation is available at https://jakubn.gitlab.io/howtonotgetinsane

jakubn@gmail.com

https://nabrdalik.dev

twitter @jnabrdalik

@jnabrdalik@mstdn.social


